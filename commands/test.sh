#!/usr/bin/env bash

IMAGE_NAME="${IMAGE_NAME:-pipeline-base-image:latest}"

main() {
      test_image
      report_results
}

test_image() {
      assert_is_alpine
      assert_command_exists bash
      assert_command_exists git
      assert_command_exists docker
}

assert_is_alpine() {
      echo "assert_is_alpine"
      assert_command_exists apk
}

FAILURE=false
assert_command_exists() {
      echo "assert_command_exists: $1"
      if ! docker run --rm "$IMAGE_NAME" which "$1" &>/dev/null ; then
            echo >&2 "FAIL: Command not installed: $1"
            FAILURE=true
      fi
}

report_results() {
      if [[ "$FAILURE" == true ]] ; then
            echo "ERROR: One or more tests failed."
            return 1
      else
            echo "SUCCUSS: All tests passed."
            return 0
      fi
}

main
