#!/usr/bin/env bash

IMAGE_NAME="${IMAGE_NAME:-pipeline-base-image:latest}"
docker build --tag "${IMAGE_NAME}" ./source/
